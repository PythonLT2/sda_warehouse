Source code for a Item(Warehouse?) management system

To start:
    
1. Make sure you have poetry setup: https://python-poetry.org/
2. Clone/Download project
3. In project root type `poetry install` to get all project dependencies.
4. To run type into terminal: `python manage.py runserver`