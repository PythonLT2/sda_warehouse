from django.contrib import admin

# Register your models here.
from sda_warehouse.models import Item, WarehouseUser

# Item modelio registracija
admin.site.register(Item)
admin.site.register(WarehouseUser)
