from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect


def unauthenticated_user(view):
    def wrapper(request: HttpRequest, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('/')
        else:
            return view(request, *args, **kwargs)

    return wrapper


def authenticated_user(view):
    def wrapper(request: HttpRequest, *args, **kwargs):
        return redirect('/') if not request.user.is_authenticated else view(request, *args, **kwargs)

    return wrapper


def allowed_users(allowed_roles=[]):
    def wrapper(view):
        def wrapper_func(request: HttpRequest, *args, **kwargs):
            group = None
            if request.user.groups.exists():
                group = request.user.groups.all()[0].name

            if group in allowed_roles:
                return view(request, *args, **kwargs)
            else:
                return HttpResponse("<p>You do not have a permission to view this page</p>")
        return wrapper_func
    return wrapper
