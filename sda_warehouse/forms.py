from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.forms import CharField, TextInput
from sda_warehouse.models import Item, WarehouseUser


class UserRegistrationForm(UserCreationForm):
    first_name = CharField(label='First Name', widget=TextInput(attrs={"placeholder": "Your name"}), required=True)
    last_name = CharField(label='Last Name', widget=TextInput(attrs={"placeholder": "Your last name"}), required=True)
    email = CharField(label="Email address", widget=TextInput(attrs={'class': 'email-field', 'type': 'email'}),
                      required=False)
    username = CharField(label='Username', widget=TextInput(attrs={"placeholder": "Your username"}), required=True)

    class Meta:
        model = WarehouseUser
        fields = ['first_name', 'last_name', 'email', 'username', 'roles']


class LoginForm(AuthenticationForm):
    username = CharField(label='Username', widget=TextInput(attrs={"class": "usrname-fld", 'type': 'text'}),
                         required=True)
    password = CharField(label='Password', widget=TextInput(attrs={'class': 'password-field', 'type': 'password'}),
                         required=True)

    class Meta:
        model = WarehouseUser
        fields = ['username', 'password']


########################################################################################################


class ItemForm(forms.ModelForm):

    class Meta:
        model = Item
        fields = [
            "item_name",
            "unit_of_measure",
            "quantity",
            "price",
            "item_type",
            "status"
        ]
