from decimal import Decimal

from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models.fields import CharField


class WarehouseUser(User):
    ROLES = (
        ("admin", "Admin"),
        ("staff", "Staff"),
        ("customer", "Customer"),
    )

    roles = CharField(max_length=20, choices=ROLES)


class Item(models.Model):
    UNIT_OF_MEASURE = (
        ("Units", "Units"),
        ("Kilogrames", "Kilogrames"),
        ("Litres", "Litres"),
        ("Square meters.", "Square meters"),
        ("Cubic meters", "Cubic meters"),
    )

    STATUS = (
        ("Available", "Available"),
        ("Not Available", "Not Available"),
        ("Pending", "Pending"),
        ("Out of Stock", "Out of Stock"),
    )

    item_name = models.CharField(max_length=200, blank=False)
    unit_of_measure = models.CharField(max_length=20, choices=UNIT_OF_MEASURE)
    quantity = models.DecimalField(max_digits=10000, decimal_places=3,
                                   validators=[MinValueValidator(Decimal("0.001"))])
    price = models.DecimalField(max_digits=10000, decimal_places=2,
                                validators=[MinValueValidator(Decimal("0.01"))])
    item_type = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=20, choices=STATUS)

    class Meta:
        db_table = "item"
