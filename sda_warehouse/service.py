import csv

from django.contrib.auth.models import User, Group, Permission
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render

from sda_warehouse.models import Item


def create_group(user: User, group_name: str):
    group, created_group = Group.objects.get_or_create(name=group_name)

    if group or created_group:
        user.groups.add(group)


def does_user_exist(request: HttpRequest):
    user = User.objects.filter(username=request.POST['username'])
    return user.exists()


def does_item_exist(request: HttpRequest):
    item = Item.objects.filter(item_name=request.POST['item_name'])
    return item.exists()


def export(request):
    response = HttpResponse(content_type="text/csv")

    writer = csv.writer(response)
    writer.writerow(["Item Name", "Quantity", "Price", "Item type", "Status"])

    for item in Item.objects.all().values_list("item_name", "quantity", "price", "item_type", "status"):
        writer.writerow(item)

    response["Content-Disposition"] = 'attachment; filename="warehouse.csv"'

    return response
