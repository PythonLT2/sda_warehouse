"""sda_warehouse URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView

from sda_warehouse.views import login_view, register_view, logout_view, item_request, show, edit, deletex, item_read
# update,
from sda_warehouse.service import export

urlpatterns = [
    path('', TemplateView.as_view(template_name='home.html'), name='homepage'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('signup/', register_view, name='signup'),
    path('item/<str:id>', item_read, name='item-read'),
    path("item_request/", item_request),
    path("show/", show, name="show"),
    path("edit/<str:id>/", edit),
    # path("update/", update),
    path("delete/<str:id>/", deletex),
    path("export/", export),
    path('admin/', admin.site.urls),

]
