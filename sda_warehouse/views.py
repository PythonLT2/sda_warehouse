from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.core.paginator import Paginator
from django.http import HttpRequest
from django.shortcuts import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
from django.db.models import Q

from sda_warehouse.decorators import unauthenticated_user, authenticated_user, allowed_users
from sda_warehouse.forms import UserRegistrationForm, LoginForm, ItemForm
from sda_warehouse.models import Item
from sda_warehouse.service import create_group, does_item_exist


@unauthenticated_user
def home(request: HttpRequest):
    return render(request, "home.html", {})


# User login/logout/creation view functions

@unauthenticated_user
def login_view(request: HttpRequest):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
                messages.success(request, "You have successfully logged in!")
                return redirect('/')
        else:
            messages.error(request, "Invalid login credentials! \nTry again. Or make a new account")
    else:
        form = LoginForm()
    return render(request, 'login.html', {
        "form": form
    })


@authenticated_user
def logout_view(request: HttpRequest):
    logout(request)
    messages.success(request, "You have successfully logged out")
    return redirect('/')


@unauthenticated_user
def register_view(request: HttpRequest):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            group_name = request.POST['roles'].lower()
            create_group(user, group_name)
            messages.success(request, "You have successfully created your account")
            return redirect('/')
    else:
        form = UserRegistrationForm()
    return render(request, 'signup.html', {
        "form": form,
    })


# Warehouse item view functions

@authenticated_user
@allowed_users(allowed_roles=['admin', 'staff'])
def item_request(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid() and not does_item_exist(request):
            form.save()
            messages.success(request, "Item successfully created")
            return redirect('/show')
        else:
            if does_item_exist(request):
                messages.info(request, "Item with that name already exists")
            return redirect('/item_request')
    else:
        form = ItemForm()
    return render(request, 'index.html', {'form': form})


@authenticated_user
def item_read(request, id):
    item = get_object_or_404(Item, id=id)
    return render(request, "item.html", {
        "item": item
    })


class ItemList(ListView):
    paginate_by = 2
    model = Item


@authenticated_user
def show(request):
    search_post = request.GET.get("search")
    if search_post:
        items = Item.objects.filter(Q(item_name__icontains=search_post)
                                    | Q(quantity__icontains=search_post)
                                    | Q(price__icontains=search_post)
                                    | Q(item_type__icontains=search_post)
                                    | Q(status__icontains=search_post))
    else:
        items = Item.objects.all().order_by("item_name")
    paginator = Paginator(items, 10)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, "show.html", {
        "page_obj": page_obj,
    })


@authenticated_user
@allowed_users(['admin', 'staff'])
def edit(request, id=id):
    item = get_object_or_404(Item, id=id)
    form = ItemForm(request.POST or None, instance=item)
    if form.is_valid():
        form.save()
        return redirect('/show')
    context = {
        "form": form
    }
    return render(request, 'update.html', context)

@authenticated_user
@allowed_users(['admin', 'staff'])
def deletex(request, id):
    context = {}
    item = get_object_or_404(Item, id=id)
    if request.method == "POST":
        item.delete()
        return HttpResponseRedirect("/show")

    return render(request, "delete.html", context)
