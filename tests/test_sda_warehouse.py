from django.test import Client

import pytest

from sda_warehouse.models import WarehouseUser, Item


@pytest.fixture
def item_fixture():
    return Item.objects.create(
        item_name="Test Item",
        unit_of_measure=("kgs", "Kilogrames"),
        quantity="2",
        price="3.50",
        item_type="Misc",
        status=("Available", "Available")
    )


def login(client: Client) -> WarehouseUser:
    client.logout()
    user = WarehouseUser.objects.create_user(
        username="foo",
        password="bar",
        first_name="foo_first",
        last_name="bar_last",
        roles="staff"
    )
    client.login(username=user.username, password='bar')
    return user


def customer_login(client: Client):
    client.logout()
    another_user = WarehouseUser.objects.create_user(
        username="john",
        password="doe",
        first_name="foo_first",
        last_name="bar_last",
        roles="customer"
    )
    client.login(username=another_user.username, password=another_user.password)
    return another_user


def test_homepage(client: Client):
    resp = client.get('/')
    assert b"Temporary homepage for Warehouse app" in resp.content


@pytest.mark.django_db
def test_login(client: Client):
    login(client)
    resp = client.get("/login/")
    assert resp['location'] == '/'


@pytest.mark.django_db
def test_logout(client: Client):
    resp = client.get("/logout/")
    assert resp['location'] == '/'


@pytest.mark.django_db
def test_user_creation(client: Client):
    resp = client.post("/signup/", html=True, data={
        "username": "foo",
        "password1": "hello_world",
        "password2": "hello_world",
        "first_name": "foo_first",
        "last_name": "bar_last",
        "email": "",
        "roles": "staff"
    })
    found_user = WarehouseUser.objects.get(username="foo")
    assert found_user.username == "foo"
    assert resp['location'] == '/'
    assert found_user.roles == 'staff'


def test_entering_register_page(client: Client):
    resp = client.get("/signup/")
    assert resp.status_code == 200
    assert b'Register' in resp.content


@pytest.mark.django_db
def test_item_request(client: Client):
    login(client)
    resp = client.get("/item_request")
    assert resp['location'] == '/item_request/'

    resp = client.post("/item_request", html=True, data={
        "item_name": "Test Item",
        "unit_of_measure": "Kilogrames",
        "quantity": "2",
        "price": "3.50",
        "item_type": "Misc",
        "status": "Available"
    })
    assert resp.status_code == 301
    assert resp['location'] == '/item_request/'


@pytest.mark.django_db
def test_item_show(client: Client):
    login(client)
    resp = client.get("/show/")
    assert resp.status_code == 200

    client.get("/logout/")
    resp = client.get("/show/")
    assert resp.status_code == 302


@pytest.mark.django_db
def test_item_read(client: Client, item_fixture):
    login(client)
    resp = client.get(f"/item/{item_fixture.id}")
    assert item_fixture is not None
    assert resp.status_code == 200

    customer_login(client)

    resp = client.get(f"/item/{item_fixture.id}")
    assert resp.status_code == 302
    assert resp['location'] == '/'

@pytest.mark.django_db
def test_edit(client: Client, item_fixture):
    login(client)
    resp = client.get(f"/edit/{item_fixture.id}")
    assert item_fixture is not None

    resp = client.post(f"/edit/{item_fixture.id}", html=True, data={
        "item_name": "Test Item changed",
        "unit_of_measure": "Units",
        "quantity": "3",
        "price": "5.50",
        "item_type": "Something",
        "status": "Pending"
    })
    assert resp.status_code == 301
    assert resp['location'] == f'/edit/{item_fixture.id}/'

@pytest.mark.django_db
def test_delete(client: Client, item_fixture):
    login(client)
    resp = client.get(f"/delete/{item_fixture.id}")
    assert resp.status_code == 301
